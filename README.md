  Core javascript concepts 
  
  - Process map
  - Global/Local dictionaries
  - Adding removing variables dynamically
  - 5 data types (+1)
  - Object internals
  - 5 ways of creating objects 
  - Function internals
  - Difference between __proto__ and prototype
  - Prototype chaining
  - Internal differences between  function and lambda
  - Inheritance using functions and classes
  - Internals of array and array methods
  - Closures
  - Lack of async in JavaScript
  - setTimeout, setInterval
  - Promise internals
  - Understanding async and await